﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private CharacterController _controller;
    [SerializeField] private float _speed = 3.5f;
    [SerializeField] private float _gravity = 9.81f;

    [SerializeField] private GameObject _muzzleFlash;
    [SerializeField] private GameObject _hitMarkerPrefab;
    [SerializeField] private AudioSource _weaponAudio;

    private int _currentAmmo;
    [SerializeField] private int _maxAmmo = 50;

    private bool _isReloading = false;

    private UIManager _uIManager;

    //var for has coin
    public bool hasCoin = false;

    [SerializeField] private GameObject _weapon;

    // Start is called before the first frame update
    void Start()
    {
        _controller = GetComponent<CharacterController>();
        //hide mouse cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        _currentAmmo = _maxAmmo;

        _uIManager = GameObject.Find("Canvas").GetComponent<UIManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //if left click
        //cast ray from centre point of main cam

        if (Input.GetMouseButton(0) && _currentAmmo > 0)
        {
            Shoot();
        }
        else
        {
            _muzzleFlash.SetActive(false);
            _weaponAudio.Stop();
        }

        if (Input.GetKeyDown(KeyCode.R) && (_isReloading == false))
        {
            _isReloading = true;
            StartCoroutine(Reload());
        }

        CalculateMovement();
        //if escape pressed unhide mouse cursor
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    private void Shoot()
    {
        _muzzleFlash.SetActive(true);
        _currentAmmo--;
        _uIManager.UpdateAmmo(_currentAmmo);
        //if audio is not playing
        //play audio
        if (_weaponAudio.isPlaying == false)
        {
            _weaponAudio.Play();
        }


        Ray rayOrigin = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        RaycastHit hitInfo;

        if (Physics.Raycast(rayOrigin, out hitInfo))
        {
            Debug.Log("Hit: " + hitInfo.transform.name);
            GameObject hitMarker = Instantiate(_hitMarkerPrefab, hitInfo.point, Quaternion.LookRotation(hitInfo.normal));
            Destroy(hitMarker, 1f);

            //check if we hit the crate
            //call destroy crate
            Destructible crate = hitInfo.transform.GetComponent<Destructible>();
            if (crate)
            {
                crate.DestroyCrate();
            }
        }
    }

    private void CalculateMovement()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        Vector3 direction = new Vector3(horizontalInput, 0, verticalInput);
        Vector3 velocity = direction * _speed;
        velocity.y -= _gravity;

        velocity = transform.TransformDirection(velocity);

        _controller.Move(velocity * Time.deltaTime);
    }

    IEnumerator Reload()
    {
        yield return new WaitForSeconds(1.5f);
        _currentAmmo = _maxAmmo;
        _uIManager.UpdateAmmo(_currentAmmo);
        _isReloading = false;
    }

    public void EnableWeapons()
    {
        _weapon.SetActive(true);
    }
}
