﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private AudioClip _coinPickup;

    //check for collision
    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                Player player = other.GetComponent<Player>();
                if (player)
                {
                    player.hasCoin = true;
                    AudioSource.PlayClipAtPoint(_coinPickup, transform.position, 1f);
                    UIManager uIManager = GameObject.Find("Canvas").GetComponent<UIManager>();
                    if (uIManager)
                    {
                        uIManager.CollectedCoin();
                    }
                    Destroy(gameObject);
                }
            }
        }
    }
    //check if it's player
    //check for e key press
    //if it is player, tell player it's the coin
    //play coin sound
    //destroy the coin
}
